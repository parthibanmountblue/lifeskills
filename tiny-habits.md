## Question 1
## Your takeaways from the video (Minimum 5 points)

* Real change comes through hundreds of small decisions of small habits.
* One can make drastic changes with minor tweaks to the daily routine. It does not require something big.
* We generally underestimate the power of repetitively making smaller improvements over a longer period.
* Deciding the change needed into tiny habits, and adopting them one after another gradually.
* To develop a habit, have motivation, ability and a trigger.

## Q2. Your takeaways from the video in as much detail as possible
To develop a habit, cultivate tiny habits, Which can be done by following 3 process.
* Shrink the behaviour/Take the first step: Shrink the behaviour into tinier habit which  requires little motivation and has bigger impact.

* Identify an action prompt: There are three types of prompts - External, Internal and Action prompt. Use action prompt with regular behavior to trigger a new behavior rather than external or internal prompt.

* Grow habits with some shine: Celebrate after every small victories, which increases the confidence and motivation to make progress.

## 3. How can you use B = MAP to make making new habits easier?
* B(Behaviour) = MAP(Motivation, Ability, Prompt)
* This formula says, if you are trying to develop new habbit,
* Reduce the quantity(Break into smaller chunk)/ Just do the first step.
* Identify an action prompt, and attach it to the habit
* Celebrate the small victories.

## Q4. Why it is important to "Shine" or Celebrate after each successful completion of habit?
* Celebrating small victories and rewarding ourselves attracts us towards the habit of continuing it for the long term and thus helps us become a better version of ourselves.

## Q5. Your takeaways from the video (Minimum 5 points)

* Habits develop and get internalise over time.
* Create barriers for your bad habbits.
* Make good habbits easier to follow.
* Make a proper plan and have clarity for achieving any behaviour.
* Environment affect one's behaviour, so look for environment that is condusive.
* Plan for Optimizing the beginning, not the ending.
* Follow Two-minute rule: If it takes two minutes or less, do it instantly, don't procastinate it.

## Q6. Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

* Outcomes are about what you get. Processes are about what you do. Identity is about what you believe.
* With outcome-based habits, the focus is on what you want to achieve. With identity-based habits, the focus is on who you wish to become.
* The ultimate form of intrinsic motivation is when a habit becomes part of your identity.
* Decide the type of person you want to be. Prove it to yourself with small wins.
* The most effective way to change your habits is to focus not on what you want to achieve, but on who you wish to become.
* Becoming the best version of yourself requires you to continuously edit your beliefs, and to upgrade and expand your identity.
## Q7. Write about the book's perspective on how to make a good habit easier?
* A good habit can be cultivated in four stages:
* Cue: Initiate the action, make the first move. Make the trigger obvious and easy to reach.
* Craving: Have cravings which attract you towards it and make it look effortless.

* Response: Make the habit easier by creating fewer steps.

* Reward: By Rewarding yourself for small victories and celebrating it.

## Q8. Write about the book's perspective on making a bad habit more difficult?
A habit can be made more difficult by

* Making the habit harder.
* Making it ugly to indulge in the behavior.
* Increasing the friction of the behaviour.
* Making the behavior immediately unsatisfying.


## Q9. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
* Exercise daily
## Q10. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
* Avoiding cell phones after 10pm.
