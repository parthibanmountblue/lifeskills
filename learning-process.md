# How to Learn Faster with the Feynman Technique

## 1)What is the Feynman Technique? Paraphrase the video in your own words.


###  Feynman Technique:


* Feynman Technique is a method of learning that promotes deep understanding of concepts.
* This method was developed by Nobel prize winning physicist Richard Feyman.
* It involves explaining the concept in simple words as if we were to explain to a child.
* There are four key steps in Feynman Technique:
>  1) Take a paper and write the name of concept.
>  2) Explain the concept as simple as possible.
>  3) Revisit the source and find struggling areas if any.
>  4) Challenge yourself to simplify it further.



## 2)What are the different ways to implement this technique in your learning process?
* Explain the concepts in simple words to a friend.
* Write it down in a paper in simple english.
* Explain it to a person who is new to this concept in simple words.
* Revisit sources.


# Learning How to Learn TED talk by Barbara Oakley

## 3)Paraphrase the video in detail in your own words.
* Brain has two modes - Focus mode , Diffuse mode.
* In focus mode we have complete focus on things we do.
* New ideas emerge in diffuse mode.
* Our brain should jump between these two modes,bringing new ideas from diffuse mode and focussing on things in focus mode.
* Procastination is addictive but it can be overcome by Pomodoro Technique.
* Pomodoro Technique involves complete focus for 25 minutes followed by a small break.
* Recalling is essential for effective learning.

## What are some of the steps that you can take to improve your learning process?

* Follow Pomodoro Technoque - set a timer for 25 minutes and take a short break after it.
* Recall things that you have learned.


#  Learn Anything in 20 hours

## Your key takeaways from the video? Paraphrase your understanding.

* Learning new things is an exponential curve.
* Initially we would be bad at it but with practise we can improve it.
* It takes 20 hours of learning to be good at a skill.
* Focus on basics of skill - deconstruct the skill.

## What are some of the steps that you can take while approaching a new topic?
* Break a skill into chapters.
* overcome initial frustation period.
* Focus on basics.



