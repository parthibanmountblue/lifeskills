# 1. Grit

## Question 1
 ## Paraphrase (summarize) the video in a few lines. Use your own words.

 * Performace of a person doesnot depend entirely on a persons IQ.
 * One important characteristics which makes a person success is grit.
 * Grit is passion and preserverance for a very long time.
 * Grit is living life as if it is a marathon.
 * Best way for building grit is growth mindset.
 * Growth mindset is the belief that ability to learn is not fixed and it can be improved.


# Question 2
## What are your key takeaways from the video to take action on?
* Performace of a person doesnot depend entirely on a persons IQ.
* Grit is passion and preserverance for a very long time.
* Growth mindset is the belief that ability to learn is not fixed and it can be improved.


 # Question 3
Paraphrase (summarize) the video in a few lines in your own words.
* People with fixed mindset believes that skills and talents are predefined ,you either have them or not.
* They believe peoples donot have a control on their ability.
* People with growth mindset believes that ability and skill can be improved over time.
* They believe peoples do have a control on their ability.
* Four key ingredients to growth-effort,challenges,mistakes and feedback.
* A person can have a growth mindset at some point of time and fixed point of time,it is a spectrum.

# Question 4
## What are your key takeaways from the video to take action on?
* Approach everything in a growth mindset.
* Effort,challenges,mistakes and feedback is important.

# Question 5
## What is the Internal Locus of Control? What is the key point in the video
* Locus of control is the degree to which you believe you have control over things.
* Internal locus of control is believeing outcome is based on a persons effort and hard work.
* People with Internal locus of control have increased level of motivation.
* We can develop Internal locus of control by appreciating the fact that our hardwork is responsible for outcome.

# Question 6
## Paraphrase (summarize) the video in a few lines in your own words.

* Believe in your ability to figure things out.

* Challenge our presumptions and pessimistic attitudes question your assumptions
* Be open to new ideas 

* Create your own curriculum for growth

* Honor the struggle

# Question 7
## What are your key takeaways from the video to take action on?

* Change your belief that difficulties are unpleasant.
* Challenge our pessimistic attitude.
* Have a  clear, doable goal.
* Give enough time.

# Question 8
## What are one or more points that you want to take action on from the manual? (Maximum 3)
* I will stay with a problem till I complete it. I will not quit the problem.
* I know more efforts lead to better understanding.
