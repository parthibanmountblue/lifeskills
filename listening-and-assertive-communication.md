## Question 1

## What are the steps/strategies to do Active Listening? (Minimum 6 points)

* Face the speaker and have eye contact.
* Don't interrupt.
* Listen without judging, or jumping to conclusions.
* Dont get distracted.
* Show that you're listening.
* Stay focused.


## Question 2

## According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

Listening is following the thoughts and feelings of another and understanding what the other is saying from his or her perspective. Reflective listening is hearing and understanding, and then letting the other know that he or she is being heard and understood.

* It lets the speaker know that she or he has been heard, understood and supported.
* It allows you to check your own accuracy in hearing what the other has said.

## Question 3

## What are the obstacles in your listening process?

### External Listening Barriers:
They include a variety of environmental distractions that can usually be avoided or minimized with simple corrections. External barriers include:

* Noise - Sound from outside,other people talking.
* Visual distractions - Things which distracts from seeing speaker.
* Physical setting.

### Internal Listening Barriers:
Internal listening barriers are more difficult to manage, as they reside inside the mind of the listener.Internal barriers include:

* Anxiety. 
* Mental laziness.
* Boredom. 


## Question 4

## What can you do to improve your listening?

* Consider eye contact.
* Make a mental image of what the speaker is saying.
* Empathise with the speaker.
* Provide feedback.
* Keep an open mind.

## Question 5

## When do you switch to Passive communication style in your day to day life?
The passive communication style is often used by those who want to come off as indifferent about the topic at hand.
I use passive communication during,
* When I am not satisfied with their services.
* speaking with elder people.
* Opinion on topics.

## Question 6
## When do you switch into Aggressive communication styles in your day to day life?
* When someone is constantly disturbing me.
* when I get offended.

## Question 7
## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* While talking with people I am comfortable with.
* Friends and Family.

## Question 8
## How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.

* choose the right time to bring up an issue.
* Be direct. Be direct in your conversation, do not beat around the bush.
* Use body language to emphasize your words.
