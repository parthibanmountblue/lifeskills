## Q1. What is Deep Work?

* Focusing without distraction on a demanding task is called as Deep Work.
* Deep work increases quality of work.

## Q2. Paraphrase all the ideas in the above videos and this one in detail.

* Optimal duration for deep work is at least 1 hour. Not to switch between context for at least an hour during deep work.
* Deadlines/time-block schedules are kind of motivational signal, it helps one by avoiding procastinating things and avoid taking unneccessary breaks in middle.
* Deadlines should be reasonale to humane capacity, too much pressure can reduce the productivity.
* Breaks and distractions must be scheduled.
* Deep work must be made as a habbit.


## Q3. How can you implement the principles in your day to day life?

* Practice deep work with atleast one hour at a stretch, without switching context.
* Don't look at smartphones unneccessarily which wastes a lot of time, and reduce concentration.
* Having adequate sleep.


## Q4. Your key takeaways from the video

* Social media is not a fundamental technology.
* Social medias offers shiny treats in exchange of minutes of user's attention.
* Many of the social media companies hire attention engineers, who gather principles from gambling, and other such, and make these products as addictive as possible.
Focus on doing deep, concentrated work required to build real skills and to apply those skills to produce things that are valuable.
* social media can reduce our Ability to sustain concentration & Harm our professional success: as social medias are designed to be addictive, so much of the attention is lost to this. This permanently reduces the capacity to concentrate, and do more neccessary in a competitive economy.
* Social media can affect us physicologically, more the usage of social media, the more likely to feel loney or isolated.
* It may feel discomfort after leaving social media, for around two weeks, but it's a true detox process.