## Question 1
### What is your one major takeaway from each one of the 6 sections. So 6 points in total.
* Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page.
* Requirements have changed? - If the current deadline will not be met due to the changes, inform relevant team members about the new deadline.
* Explain the problem clearly, mention the solutions you tried out to fix the problem.
* Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.
* Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.
* Programming is essentially a result of your sustained and concentrated attention

## Question 2
### Which area do you think you need to improve on? What are your ideas to make progress in that area?

* The way you ask a question determines whether it will be answered or not. You need to make it very easy for the person to answer your question. Messages like the database is not connecting, need your help or the build is not working, can you help me out, will not get you the answers you are looking for.