
# Messaging Queues :
Message Queuing System is a service that separates the act of sending a message from the act of receiving that message. It automates certain non-essential task thereby reducing traffic and workload. Messages received from the sender are stored in a temporary buffer (Queue) until the receiver can receive and process the messages.


# Uses of Message Queuing:
Messaging Queues can help in,

1)Sending emails:
    Lets say a company wants to send many emails to different users regarding offers,sales,campaign,password reset. Messaging queues can help to automate it by, creating "producer" service which generate emails and push email objects into the queue. A single consumer microservice, dedicated to sending emails, consumes messages from the queue one by one and sends emails as specified in the email object.


![Message queus in email](https://sharmarajdaksh.github.io/static/8ce68cbc116a7969c676d88e8c41b33e/1ac66/email.png)

2)Data post processing:
    Lets say  you have a Blogging application which needs to handle large amounts of image data, from images uploaded by users themselves. Users can't be expected to provide images which are web-optimized or small in size. One solution is to post-process and optimize all images that are uploaded by the user.  A service may be employed in the application architecture whose sole purpose is optimizing images that get uploaded on the application. A message queue can help in this scenario. we can create a queue which gets image from users and optimize them as per specifications given.
# Advantages:
1) Asynchronous Processing - With a message queuing system, the producer sends a message to the system and then continues to produce next message without synchronizing with the consumer side, which avoids wasting resources while waiting for the feedback. 
    
2) Decoupling of Producers and Consumers - With a message queuing system as middleware, the producer is only responsible for publishing the message to the queue without any consideration of response from receivers.Thus saving precious time and resource.

3) Peak Shaving - Sometimes servers can crash because of too many requests in a short time. A message queuing system can be introduced to buffer the data, therby preventing server crash.

# Popular tools:
There are many message queuing systems some of the widely used tools are,

    1) Kafka
    2) RabbitMQ 
    3) Celery
    4) Nsq
    5) Pulsar
    6) IBM MQ

## Kafka:
Kafka is a distributed, separable, redundant, and persistent logging service developed by Scala based on the TCP protocol. Kafka has an active community, which makes it more and more popular these days.

 In Kafka, messages are classified into different Topics, and each Topic can be divided into multiple partitions, which are distributed and stored on different Brokers (a cluster of worker machine). By introducing the partition concept, the messages are uniformly distributed to multiple partitions, which help improve the parallelism of the system. In addition, multiple consumers can simultaneously read messages from one or more partitions to improve parallel processing capabilities.
 ## RabbitMQ:
 RabbitMQ is an open source implementation of AMQP protocol developed with Erlang(a multipurpose programming language for developing concurrent and distributed systems). Erlang inherently supports distributed computation, so RabbitMQ does not rely a third-party cluster manager.
 ## Celery:
 Celery is an open source, reliable distributed message queue system to process vast amounts of messages. It is a task queue with focus on real-time processing while it also supports task scheduling. Celery is licensed under the BSD License. Celery has a simple asynchronous process queue or job queue which is based on distributed message passing. 
 ## NSQ:
 NSQ is an open source and modern real-time distributed memory best message queue designed to operate at scale. It is written in Go language and handles billions of messages per day on a large scale. NSQ message queue notification system has distributed message and decentralized topology structure. It enables fault tolerance and high availability coupled with the efficient delivery of messages.
 ## pulsar:
 Pulsar is an open-source large-scale distributed pub-sub messaging system originally created at Yahoo and now part of the Apache Software Foundation, it is developed by Java based on the TCP protocol.

 # Enterprise Message Bus:
 ![ESB](https://liveimages.algoworks.com/new-algoworks/wp-content/uploads/2016/03/09105955/How-Enterprise-Service-Bus-on-Cloud-Helps-in-Safe-Transaction-Implementation.png)
 
 An Enterprise Message Bus (EMB) is a process of integrating numerous applications together over a bus-like infrastructure. The core concept of the ESB architecture is that you integrate different applications by putting a communication bus between them and then enable each application to talk to the bus. 
 
 This decouples systems from each other, allowing them to communicate without dependency on or knowledge of other systems on the bus. The concept of ESB was born out of the need to move away from point-to-point integration, which becomes brittle and hard to manage over time. Point-to-point integration results in custom integration code being spread among applications with no central way to monitor or troubleshoot. This is often referred to as "spaghetti code"

 ## Advantages:
 1) It helps to combine data from different system and acts as a single system of records.
 2) It helps in quick retrieval of data because of cache.
 3) It can provide uninterrupted data to system even when one or more source systems are down for maintanence.
 4) Acts as an additional layer of security.
 
 # Reference section:
 * Basics of  [Message Queues](https://sharmarajdaksh.github.io/blog/the-what-why-and-how-of-message-queues). 
 * Important [Tools of MQ](https://blog.containerize.com/2021/07/09/top-5-open-source-message-queue-software-in-2021/)
 * Enterprise Message Bus [EMB](https://www.youtube.com/watch?v=cW9SiCH0kYs)



