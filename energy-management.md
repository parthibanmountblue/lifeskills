## What are the activities you do that make you relax - Calm quadrant?


Some of the activities that make me relaxed are

* Watching movies.
* A walk outside.
* Listeing to music

## When do you find getting into the Stress quadrant?
* when I am not prepared/ready to meet the situation.
## How do you understand if you are in the Excitement quadrant?

* when i meets my own expectation.
* when i am satisfied with my work.

## Q4. Paraphrase the Sleep is your Superpower video in detail.


* Lack of sleep will age a man by a decade.
* We need sleep after studying, to save it in brain, as welll as before studying to prepare the brain to study(to absorb new information). Lack-of-sleep can lead to learning disability.
* It was found in a research that, sleep shifts memory vulnerable reservior to a more long-term storage space in the brain.
* There is a physiological signature of aging with lack of sleep.
* Sleep loss can cause  Cardio-vascular diseases.
As per a research, it was found that, due to day-light-saving mechanism in cold-temperate countires, In spring, when they lose one-hour of sleep, there is an increase of 21% Heart attacks.
* Similar kind of data was found for car-crashes, and even sucides rates as well.
* Lack of sleep destroys natural immune cells.

* Shorter the sleep, shorter the life.

## Q5. What are some ideas that you can implement to sleep better?

* Avoiding electronic devices like mobile and laptop, one for before sleep.
* Regularity in sleep: Going to bed at same time, and  waking up at the same time. Regularity improves, quantity as well as quality of sleep.
* Sleep in relatively lower temperature room. Because, body falls to sleep better in relative-cooler environment.

## Q6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
The video explains the powerful effects of Physical Activities.

* Simply, moving your body has immediate, long-lasting and protective benefits for the brain, which lasts for rest of the life.
* Boosts our mood and energy.
* Helps to maintain focus and attention for long period.
* Physical activities increases neurotransmitters like dopamine, serotonin, and noradrenaline.
* Improves reaction time.
* Protects brain from neurodegenerative diseases.

## Q7. What are some steps you can take to exercise more?

* Exercising daily in the morning or evening.

* Taking a small walk between office and house.


