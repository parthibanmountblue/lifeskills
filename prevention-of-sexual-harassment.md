# What kinds of behaviour cause sexual harassment?

sexual harassment falls under 3 categories,
* Verbal.
* Visual.
* Physical.

Verbal harassment includes,
* comments about clothing.
* Comment on  person's body.
* Sexual or gender based jokes or remarks.
* repreatedly asking a person out.
* Spreading rumours about a person's personal life.

Visual harassment includes,
* Obscene posters.
* Drawings or Pictures.

Physical harassment includes,
* blocking movement.
* Sexual gesturing.
* Inappropriate touching.


# What would you do in case you face or witness any incident or repeated incidents of such behaviour?

Talk to the harasser: I will inform him/her about the offensive behaviour,and tell the person to stop doing it.

Complain to my supervisor: I would inform  my supervisor/HR regarding the offensive behaviour.

Follow the company's internal complaint process: I would follow the procedures and make a complaint with ICC(Internal Complaint Committee).
